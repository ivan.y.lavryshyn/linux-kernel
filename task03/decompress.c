//
// Created by ivan.y.lavryshyn on 8/3/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "term_util.h"

static void decode(const char *input_string, char *output_string) {
    char tmp;
    int i, digit;
    i = 0;
    digit = 0;
    while (input_string[i] != '\0') {
        if (isdigit(input_string[i])){
            digit = 10 * digit + (input_string[i] - '0');
        } else {
            tmp = input_string[i];

            while (digit--) {
                strncat(output_string, &tmp, 1);
            }
            digit = 0;
        }
        i++;
    }
}

int main(int argc, char *argv[]) {
    if (argc < 1)
        return 0;

    int terminal_string_size = get_terminal_string_size(argc, argv);

    //printf("terminal_string_size: %d\n", terminal_string_size);

    char *terminal_string;
    terminal_string = malloc(terminal_string_size);
    terminal_string[0] = '\0';

    get_terminal_string(argc, argv, terminal_string);

    //printf("terminal_string: %s\n", terminal_string);

    char *decoded_string;
    decoded_string = malloc(1000);
    decoded_string[0] = '\0';

    decode(terminal_string, decoded_string);

    //printf("decoded_string: %s\n", decoded_string);

    puts(decoded_string);

    free(terminal_string);
    free(decoded_string);

    return 0;
}

