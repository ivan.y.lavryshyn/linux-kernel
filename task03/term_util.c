//
// Created by ivan.y.lavryshyn on 8/3/2021.
//

#include <string.h>
#include "term_util.h"

int get_terminal_string_size(int argc, char *argv[]){
    int i;
    int terminal_string_size = 0;
    for (i = 1; i < argc; i++) {
        terminal_string_size += strlen(argv[i]);
        if (argc > i + 1)
            terminal_string_size++;
    }
    return terminal_string_size;
}

void get_terminal_string(int argc, char *argv[], char *output){
    int i;
    for (i = 1; i < argc; i++) {
        strcat(output, argv[i]);
        if (argc > i + 1)
            strcat(output, " ");
    }
}


