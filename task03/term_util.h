//
// Created by ivan.y.lavryshyn on 8/3/2021.
//

#ifndef TASK03_TERM_UTIL_H
#define TASK03_TERM_UTIL_H

int get_terminal_string_size(int argc, char *argv[]);

void get_terminal_string(int argc, char *argv[], char *output);

#endif //TASK03_TERM_UTIL_H