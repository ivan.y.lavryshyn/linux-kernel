//
// Created by ivan.y.lavryshyn on 8/3/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "term_util.h"

// Function to convert integer to
// character array
char *convertIntegerToChar(int N) {
    // Count digits in number N
    int m = N;
    int digit = 0;
    while (m) {
        // Increment number of digits
        digit++;
        // Truncate the last
        // digit from the number
        m /= 10;
    }
    // Declare char array for result
    char *arr;
    // Declare duplicate char array
    char arr1[digit];
    // Memory allocation of array
    arr = (char *) malloc(digit);
    // Separating integer into digits and
    // accommodate it to character array
    int index = 0;
    while (N) {
        // Separate last digit from
        // the number and add ASCII
        // value of character '0' is 48
        arr1[++index] = N % 10 + '0';
        // Truncate the last
        // digit from the number
        N /= 10;
    }
    // Reverse the array for result
    int i;
    for (i = 0; i < index; i++) {
        arr[i] = arr1[index - i];
    }
    // Char array truncate by null
    arr[i] = '\0';
    // Return char array
    return (char *) arr;
}

static void encode(const char *input_string, int input_string_length, char *output_string) {
    int symbol_length;
    char cur, tmp;
    tmp = input_string[0];
    symbol_length = 0;
    for (int i = 0; i <= input_string_length; ++i) {
        cur = input_string[i];
        if (cur == tmp) {
            symbol_length++;
        } else {
            char *digit_array = convertIntegerToChar(symbol_length);
            strcat(output_string, convertIntegerToChar(symbol_length));
            strncat(output_string, &tmp, 1);
            free(digit_array);
            tmp = cur;
            symbol_length = 1;
        }
    }

}

int main(int argc, char *argv[]) {
    if (argc < 1)
        return 0;

    int terminal_string_size = get_terminal_string_size(argc, argv);

    //printf("terminal_string_size: %d\n", terminal_string_size);

    char *terminal_string;
    terminal_string = malloc(terminal_string_size);
    terminal_string[0] = '\0';

    get_terminal_string(argc, argv, terminal_string);

    //printf("terminal_string: %s\n", terminal_string);

    char *encoded_string;
    encoded_string = malloc(terminal_string_size);
    encoded_string[0] = '\0';

    encode(terminal_string, terminal_string_size, encoded_string);

    //printf("encoded_string: %s\n", encoded_string);

    puts(encoded_string);

    free(terminal_string);
    free(encoded_string);

    return 0;
}

