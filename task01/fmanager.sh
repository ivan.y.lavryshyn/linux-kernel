#!/bin/bash

DIR_VIEW_MODE=''
COMMAND=''
ARG_1=''
ARG_2=''
WORK_DIR=$(pwd)

if [ $# -gt 0 ]
then
    WORK_DIR=$1
fi

print_work_dir()
{
	echo Directory: $WORK_DIR
    ls $DIR_VIEW_MODE $WORK_DIR
}

print_menu()
{
	echo Menu:
    echo srt - Short View Mode
    echo ful - Full View Mode
    echo cng - Change directry
    echo cop - Copy file
    echo mov - Move/rename file
    echo del - Delete file
    echo exi - Exit
}

read_command()
{
    ARG_1=''
    ARG_2=''
	read -p 'Enter Command:' COMMAND ARG_1 ARG_2
}

refresh_screen()
{
	clear
	print_work_dir
    print_menu
    read_command
}

while [ '$COMMAND' != '' ]; do
	refresh_screen
    case $COMMAND in
        srt)
            DIR_VIEW_MODE=''
            ;;
        ful)
            DIR_VIEW_MODE='-a'
            ;;
        cng)
            if [[ -n $ARG_1 ]]
            then
                WORK_DIR=$ARG_1
            fi
            ;;
        cop)
            if [[ -n $ARG_1 && -n $ARG_2 ]]
            then
                cp $WORK_DIR/$ARG_1 $WORK_DIR/$ARG_2
            fi
            ;;
        mov)
            if [[ -n $ARG_1 && -n $ARG_2 ]]
            then
                mv $WORK_DIR/$ARG_1 $WORK_DIR/$ARG_2
            fi
            ;;
        del)
            if [ -n $ARG_1 ]
            then
                rm $WORK_DIR/$ARG_1
            fi
            ;;
        exi)
            exit 0
            ;;

    esac
done