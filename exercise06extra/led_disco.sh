echo 26 > /sys/class/gpio/export
echo 16 > /sys/class/gpio/export
echo 20 > /sys/class/gpio/export
echo 21 > /sys/class/gpio/export
sleep .5
echo in > /sys/class/gpio/gpio26/direction
echo out > /sys/class/gpio/gpio16/direction
echo out > /sys/class/gpio/gpio20/direction
echo out > /sys/class/gpio/gpio21/direction

RED=/sys/class/gpio/gpio20/value
GREEN=/sys/class/gpio/gpio16/value
BLUE=/sys/class/gpio/gpio21/value

new_value=0
old_value=0
count=0

check_button(){
new_value=$(cat /sys/class/gpio/gpio26/value)
if [[ $new_value = 1 && $new_value != $old_value ]] ; then
	let count+=1
fi
old_value=$new_value
}

start_disco(){
while [ $# -gt 0 ]
do
    sleep .5
    echo  1 > $1
    sleep .5
    echo  0 > $1
    shift
done
}

while true; do
	clear
	check_button
	echo Button pressed: $count times
	read -s -N 1 -t 0.1 key
	if [[ $count -gt 0 ]]; then
		if [ `expr $count % 2` == 0 ]
		then
			start_disco $BLUE $GREEN $RED
		else
			start_disco $RED $GREEN $BLUE
		fi
		
	fi
	if [ "$key" == $'\x0a' ]; then
		clear
		break
	fi
done


echo 26 > /sys/class/gpio/unexport
echo 16 > /sys/class/gpio/unexport
echo 20 > /sys/class/gpio/unexport
echo 21 > /sys/class/gpio/unexport