#!/bin/bash

dir='.'
depth='-maxdepth 1'
delete='-delete'
yes=''

while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -h|--help)
            echo -e 'SYNOPSIS'
            echo -e '\tdelete_files [OPTIONS] [DIRECTORY]'
            echo -e 'DESCRIPTION'
            echo -e '\t-h, --help'
            echo -e '\t-r, --recursive'
            echo -e '\t-y, --yes'
            echo -e '\t-t, --test'
            exit
            ;;
        -r|--recursive)
            depth=''
            ;;
        -t|--test)
            delete=''
            ;;
        -y|--yes)
            yes=$key
            ;;
        *)
            dir=$key
            ;;
    esac
    shift
done

find $dir $depth -regextype posix-extended -regex '.*/([_~\-].*)?(.*\.tmp)?' $delete
 
if [ "$depth" == "" ]; then
    find $dir -type d -empty $delete
fi


