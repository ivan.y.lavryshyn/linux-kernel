#!/bin/bash

rand_number()
{
	echo $((RANDOM%6))
}

res=$(rand_number)

if [[ $1 -gt $res ]]
then
	echo $1 is greater than $res
elif [[ $1 -eq $res ]]
then
	echo $1 is equal to $res
else
	echo $1 is less than $res

fi