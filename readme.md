# Repository for Linux kernel course exercises and tasks

## exercise01
Bash script for deletion of files which starts with -/_/~ or ends with .tmp

## exercise02
Bash script for appending '~' to files which 30 days old

## exercise03
Basics of git

## exercise06
Button counter

## exercise06extra
Changing colors of LED

## exercise09
Kernel modules

## task01
Simple file manager

## task02
Random number utility

## task03
RLE algorithm app

## task04
Simple program for LCD

