#!/bin/bash

echo 26 > /sys/class/gpio/export
sleep .5
echo in > /sys/class/gpio/gpio26/direction

new_value=0
old_value=0
count=0

while true; do
	clear
	new_value=$(cat /sys/class/gpio/gpio26/value)
	if [[ $new_value = 1 && $new_value != $old_value ]] ; then
		let count+=1
	fi
	old_value=$new_value
	echo Button pressed: $count times
	read -s -N 1 -t 0.1 key
	if [ "$key" == $'\x0a' ]; then
		clear
		break
	fi
done

echo 26 > /sys/class/gpio/unexport