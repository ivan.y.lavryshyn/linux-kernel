# Author
Ivan Lavryshyn

# Description

[Task #4](https://gl-khpi.gitlab.io/task04/).
Data on LCD display.

# Demo
## Counter
1. Screen with button click counter:

![Image01](res/image01.jpeg)

2. Screen after **triple** button click **X1**:

![Image02](res/image02.jpeg)

3. Screen after **double** button click **X3**:

![Image03](res/image03.jpeg)

4. Screen after **short** button click **X2**:

![Image04](res/image04.jpeg)

## Date/time
1. Date/time on screen:

![Image05](res/image05.jpeg)

2. Date/time after **5** seconds:

![Image06](res/image06.jpeg)

## IP address
1. IP address on screen:

![Image07](res/image07.jpeg)

2. Real IP address in console:

![Image08](res/image08.jpeg)