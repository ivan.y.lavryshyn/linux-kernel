#!/bin/bash

unchanged_files=`find $1 -mindepth 1 -maxdepth 1 -mtime +30 -printf '%f\n'`
for f in $unchanged_files; do
    mv $1/$f $1/~$f && ../exercise01/delete_files.sh "$(pwd)/$1"
done
