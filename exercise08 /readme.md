# Linux kernel and Buildroot compilation and running in QEMU

## Kernel:
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable -b v5.13 --depth 1

## Kernel build directory:
export BUILD_KERNEL=/home/lavrik/linux-course/linux-kernel/linux-stable/first_kernel

## defconfig for kernel:
make O=${BUILD_KERNEL} i386_defconfig

## Setup kernel:
cd ${BUILD_KERNEL}
make menuconfig

## Build kernel:
make -j6

## Buildroot:
git clone git://git.buildroot.net/buildroot

## Buildroot directory:
export BUILD_ROOTFS=/home/lavrik/linux-course/linux-buildroot/buildroot/first_buildroot

## defconfig for Buildroot:
make O=${BUILD_ROOTFS} qemu_x86_defconfig

## Setup Buildroot:
cd ${BUILD_ROOTFS}
make menuconfig

## Create user:
echo "user 1000 user 1000 =pass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS}/users

mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d
echo "user ALL=(ALL) ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user

## Create root:
mkdir -p ${BUILD_ROOTFS}/root/etc
echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells
echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells

## Build rootfs:
make -j6

## Configuration
`.config` contains current configuration.


## Install Qemu:
sudo apt-get install qemu-system-i386

## Launch Qemu with kernel and rootfs:
qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0" -drive format=raw,file=${BUILD_ROOTFS}/images/rootfs.ext3 -nic user,hostfwd=tcp::8022-:22 &

## Copy kernel module to Qemu:
scp -P 8022 ex01.ko user@localhost:~
sudo insmod ex01.ko


